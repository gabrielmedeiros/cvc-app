package br.eti.gabrielmedeiros.cvcapp;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.eti.gabrielmedeiros.cvcapp.ui.login.LoginActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> loginActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void clickConfirmButtonWithCorrectPassword_opensResultScreen() throws Exception {
        String password = "123456";

        onView(withId(R.id.edt_password)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.edt_confirm_password)).perform(typeText(password), closeSoftKeyboard());
        onView(withText(R.string.confirm)).perform(click());
        waitForSeconds(5000);
        onView(withText(R.string.successful_registration)).check(matches(isDisplayed()));
    }

    @Test
    public void clickConfirmButtonWithWrongPassword_showsErrorWarning() throws Exception {
        String password = "123456";
        String confirmPassword = "654321";

        onView(withId(R.id.edt_password)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.edt_confirm_password)).perform(typeText(confirmPassword), closeSoftKeyboard());
        onView(withText(R.string.confirm)).perform(click());
        onView(withText(R.string.passwords_are_not_the_same)).check(matches(isDisplayed()));
    }

    private void waitForSeconds(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
