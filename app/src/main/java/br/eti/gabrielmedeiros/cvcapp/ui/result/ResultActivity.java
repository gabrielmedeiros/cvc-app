package br.eti.gabrielmedeiros.cvcapp.ui.result;

import android.os.Bundle;
import android.view.MenuItem;

import br.eti.gabrielmedeiros.cvcapp.R;
import br.eti.gabrielmedeiros.cvcapp.ui.base.BaseActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResultActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.ok));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        ButterKnife.bind(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_ok)
    public void btnOkOnClick() {
        finish();
    }
}
