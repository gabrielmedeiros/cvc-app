package br.eti.gabrielmedeiros.cvcapp.utils;


import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;

public class TextUtils {

    public static SpannableString formatTextBoldAndColor(Context context, int color, String fullText, String... texts) {
        SpannableString spannableString = new SpannableString(fullText);

        for (int i = 0; i < texts.length; i++) {
            String text = texts[i];

            int textStartIndex = fullText.indexOf(text);
            int textEndIndex = textStartIndex + text.length();

            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
            spannableString.setSpan(CharacterStyle.wrap(boldSpan), textStartIndex, textEndIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(color)), textStartIndex, textEndIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        return spannableString;
    }
}
