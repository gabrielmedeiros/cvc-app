package br.eti.gabrielmedeiros.cvcapp.ui.login;


import br.eti.gabrielmedeiros.cvcapp.ui.base.BasePresenter;
import br.eti.gabrielmedeiros.cvcapp.ui.base.BaseView;

public interface LoginContract {
    interface View extends BaseView<Presenter> {
        void successfulResetPassword();
        void passwordError();
        void showError(int message);
    }

    interface Presenter extends BasePresenter {
        void resetPassword(String password, String confirmPassword);
        boolean validatePassword(String password, String confirmPassword);
    }
}
