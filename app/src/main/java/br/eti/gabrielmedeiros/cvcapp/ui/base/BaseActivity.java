package br.eti.gabrielmedeiros.cvcapp.ui.base;


import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

import br.eti.gabrielmedeiros.cvcapp.R;

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;

    @Override
    protected void onPause() {
        super.onPause();

        hideProgressDialog();
    }

    protected void showProgressDialog() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }

        this.progressDialog = null;
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setCancelable(false);
        this.progressDialog.setMessage(getResources().getString(R.string.loading));
        this.progressDialog.show();
    }

    protected void hideProgressDialog() {
        if (this.progressDialog != null && this.progressDialog.isShowing()) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }
}
