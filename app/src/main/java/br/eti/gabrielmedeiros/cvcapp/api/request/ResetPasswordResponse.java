package br.eti.gabrielmedeiros.cvcapp.api.request;


public class ResetPasswordResponse {
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
