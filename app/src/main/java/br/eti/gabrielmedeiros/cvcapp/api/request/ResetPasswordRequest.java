package br.eti.gabrielmedeiros.cvcapp.api.request;


import android.os.Parcel;
import android.os.Parcelable;

public class ResetPasswordRequest implements Parcelable {
    private String userId = "1000";
    private String newPassword;

    public ResetPasswordRequest(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeString(this.newPassword);
    }

    protected ResetPasswordRequest(Parcel in) {
        this.userId = in.readString();
        this.newPassword = in.readString();
    }

    public static final Parcelable.Creator<ResetPasswordRequest> CREATOR = new Parcelable.Creator<ResetPasswordRequest>() {
        @Override
        public ResetPasswordRequest createFromParcel(Parcel source) {
            return new ResetPasswordRequest(source);
        }

        @Override
        public ResetPasswordRequest[] newArray(int size) {
            return new ResetPasswordRequest[size];
        }
    };
}
