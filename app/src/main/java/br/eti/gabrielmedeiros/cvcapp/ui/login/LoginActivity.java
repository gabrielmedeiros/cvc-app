package br.eti.gabrielmedeiros.cvcapp.ui.login;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import br.eti.gabrielmedeiros.cvcapp.R;
import br.eti.gabrielmedeiros.cvcapp.ui.base.BaseActivity;
import br.eti.gabrielmedeiros.cvcapp.ui.result.ResultActivity;
import br.eti.gabrielmedeiros.cvcapp.utils.TextUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginContract.View {

    private LoginContract.Presenter presenter;

    @BindView(R.id.txt_title_rules)
    AppCompatTextView txtTitleRules;

    @BindView(R.id.img_password)
    AppCompatImageView imgPassword;

    @BindView(R.id.txt_error)
    AppCompatTextView txtError;

    @BindView(R.id.edt_password)
    AppCompatEditText edtPassword;

    @BindView(R.id.edt_confirm_password)
    AppCompatEditText edtConfirmPassword;

    @BindView(R.id.btn_confirm)
    AppCompatButton btnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.change_password));
        }

        ButterKnife.bind(this);

        presenter = new LoginPresenter(this);

        setupUI();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();

        super.onDestroy();
    }

    @Override
    public void successfulResetPassword() {
        startActivity(new Intent(this, ResultActivity.class));
    }

    @Override
    public void showError(int message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.app_name));
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), null);
        builder.setCancelable(true);
        builder.show();
    }

    @Override
    public void passwordError() {
        updateErrorLayout(true);
    }

    @OnClick(R.id.btn_confirm)
    public void btnConfirmOnClick() {
        if (validatePassword()) {
            String password = edtPassword.getText().toString();
            String confirmPassword = edtConfirmPassword.getText().toString();

            presenter.resetPassword(password, confirmPassword);
        } else {
            updateErrorLayout(true);
        }
    }

    private boolean validatePassword() {
        String password = edtPassword.getText().toString();
        String confirmPassword = edtConfirmPassword.getText().toString();

        return presenter.validatePassword(password, confirmPassword);
    }

    private void setupUI() {
        txtTitleRules.setText(TextUtils.formatTextBoldAndColor(this, R.color.blue_stone, getString(R.string.password_basic_rules), getString(R.string.new_password)));

        setupEvents();
    }

    private void setupEvents() {
        TextWatcher passwordTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int buttonDrawable = R.drawable.button_disabled;
                int iconPassword = R.drawable.ic_password_error;
                if (validatePassword()) {
                    buttonDrawable = R.drawable.button_enabled;
                    iconPassword = R.drawable.ic_password_ok;

                    updateErrorLayout(false);
                }

                btnConfirm.setBackground(getResources().getDrawable(buttonDrawable, null));
                imgPassword.setImageDrawable(getResources().getDrawable(iconPassword, null));
            }
        };
        edtPassword.addTextChangedListener(passwordTextWatcher);
        edtConfirmPassword.addTextChangedListener(passwordTextWatcher);
    }

    private void updateErrorLayout(boolean hasError) {
        int lineColor = R.color.shady_lady;
        int txtErrorVisibility = View.GONE;
        if (hasError) {
            lineColor = R.color.magenta;
            txtErrorVisibility = View.VISIBLE;
        }

        txtError.setVisibility(txtErrorVisibility);
        edtPassword.getBackground().mutate().setColorFilter(getResources().getColor(lineColor), PorterDuff.Mode.SRC_ATOP);
        edtConfirmPassword.getBackground().mutate().setColorFilter(getResources().getColor(lineColor), PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public void showProgressDialog() {
        super.showProgressDialog();
    }

    @Override
    public void hideProgressDialog() {
        super.hideProgressDialog();
    }
}
