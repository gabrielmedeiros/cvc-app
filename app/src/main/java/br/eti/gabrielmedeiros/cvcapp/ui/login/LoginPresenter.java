package br.eti.gabrielmedeiros.cvcapp.ui.login;


import br.eti.gabrielmedeiros.cvcapp.R;
import br.eti.gabrielmedeiros.cvcapp.api.ApiManager;
import br.eti.gabrielmedeiros.cvcapp.api.request.ResetPasswordRequest;
import br.eti.gabrielmedeiros.cvcapp.api.request.ResetPasswordResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View view;

    private ApiManager apiManager;

    public LoginPresenter(LoginContract.View view) {
        this.view = view;

        apiManager = new ApiManager();
    }

    @Override
    public void onDestroy() {
        this.apiManager = null;
        this.view = null;
    }

    @Override
    public void resetPassword(String password, String confirmPassword) {
        if (!validatePassword(password, confirmPassword)) {
            view.passwordError();
            return;
        }

        view.showProgressDialog();

        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(password);

        apiManager.getPasswordApi().resetPassword(resetPasswordRequest).enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                view.hideProgressDialog();

                if (response.isSuccessful()) {
                    view.successfulResetPassword();
                } else {
                    view.showError(R.string.error_try_again);
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                view.hideProgressDialog();

                view.showError(R.string.error_try_again);
            }
        });
    }

    @Override
    public boolean validatePassword(String password, String confirmPassword) {
        return (password.length() >= 6 && password.equals(confirmPassword));
    }
}
