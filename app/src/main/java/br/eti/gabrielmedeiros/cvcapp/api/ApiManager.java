package br.eti.gabrielmedeiros.cvcapp.api;


import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {
    private static final String BASE_URL = "https://us-central1-last-minute-app-71f82.cloudfunctions.net";

    private PasswordApi passwordApi;

    public PasswordApi getPasswordApi() {
        if(passwordApi == null) {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();

            passwordApi = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
                    .create(PasswordApi.class);
        }

        return passwordApi;
    }
}
