package br.eti.gabrielmedeiros.cvcapp.api;


import br.eti.gabrielmedeiros.cvcapp.api.request.ResetPasswordRequest;
import br.eti.gabrielmedeiros.cvcapp.api.request.ResetPasswordResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PasswordApi {
    @POST("resetPassword")
    Call<ResetPasswordResponse> resetPassword(@Body ResetPasswordRequest resetPasswordRequest);
}
