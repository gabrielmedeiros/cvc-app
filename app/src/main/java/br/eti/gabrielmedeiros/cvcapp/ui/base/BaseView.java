package br.eti.gabrielmedeiros.cvcapp.ui.base;


public interface BaseView<T> {
    void showProgressDialog();
    void hideProgressDialog();
}
